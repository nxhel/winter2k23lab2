import java.util.Scanner;
public class PartThree{
   // public static final Scanner scan= new Scanner(System.in);
    public static void main(String[] args) {
      
      Scanner scan = new Scanner(System.in);
      
      double number1=scan.nextDouble();
      double number2=scan.nextDouble();
      //static methods
      System.out.println(Calculator.addMethod(number1,number2));
      System.out.println(Calculator.multiMethod (number1,number2));
      //instance methods
      Calculator cal= new Calculator();
      System.out.println(cal.subsMethod(number1,number2));
      System.out.println(cal.diviMethod(number1,number2));
     
  }
}