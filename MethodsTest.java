public class MethodsTest{
 public static void main(String[] args) {

	System.out.println(SecondClass.addOne(50));
	SecondClass sc= new SecondClass();
	System.out.println(sc.addTwo(50)); 
//  String s1= "java";
//  String s2= "programming";
//  System.out.println(s1.length()+ " ... "+ s2.length());
	int x=5;
	methodOneInputNoReturn(x+10);
 }
 
 //question 8
  public static double sumSquareRoot( int num1, int num2) 
  {
    int sum= num1+num2;
    return Math.sqrt(sum);
  }
 
  //question7 
 public static int methodNoInputReturnInt()
 {
   int num=5;
   return num;
 }
 
  //question 6
 public static void methodTwoInputNoReturn(int number1, double number2)
 {
   System.out.println(number1+ " " + number2);
 }
 
 public static void methodOneInputNoReturn(int number)
 {
   System.out.println("Inside the method one input no return");
   System.out.println(number);                     
 }
 
 public static void methodNoInputNoReturn()
 {
   int x=50;
   System.out.println("I’m in a method that takes no input and returns nothing");
 }
}